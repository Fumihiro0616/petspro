﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropTrapScript : MonoBehaviour
{
    Vector3 basicPosition;

    //震えてるかフラグ
    bool shakeFlag = false;
    //震える時間
    float shakeTimer = 0;

    // Start is called before the first frame update
    void Start()
    {
        //最初の位置を覚えておく
        basicPosition = transform.position;
        
    }

    // Update is called once per frame
    void Update()
    {
        //震えてる
        if (shakeFlag == true)
        {
            float x = Random.Range(-0.05f, 0.05f);
            transform.position = basicPosition + new Vector3(x, 0, 0);  //最初の位置からランダムでずらす

            //タイマーをカウントダウン
            shakeTimer -= Time.deltaTime;

            //タイマーが0を切ったら
            if (shakeTimer < 0)
            {
                //通常
                shakeFlag = false;
                //Rigidbody2Dを後付けすることで落下する
                this.gameObject.AddComponent<Rigidbody2D>();
            }          
        }
    }

    //このメソッドが呼ばれれば震えスタート
    public void Shake()
    {
        shakeFlag = true;
        shakeTimer = 1.0f;
    }

    //プレイヤーに当たれば震える
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Shake();
        }
    }
}
