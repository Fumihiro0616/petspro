﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PetsStatus : IPetsInterface
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void IPetsInterface.Shoot() { }
    void IPetsInterface.AddDamege(int attack, int defense) { }
    void IPetsInterface.Skill() { }

}
