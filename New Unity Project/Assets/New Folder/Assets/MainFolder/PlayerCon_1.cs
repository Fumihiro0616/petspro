﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCon_1 : MonoBehaviour
{
    SpriteRenderer MainSpriteRenderer;

    public Sprite CatSprite;
    public Sprite DogSprite;
    public AudioClip hit;
    AudioSource audio;

    Rigidbody2D rigid2D;
    bool roop=false;
    float jamp = 350.0f;
    bool floorTaech = false;
    int charactorNumber = 0;
    float localScale = 0.19f;
    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
        rigid2D = GetComponent<Rigidbody2D>();
        charactorNumber = SelectCharactorScript.Player_1SelectCharactor();
        MainSpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        if (charactorNumber==0) MainSpriteRenderer.sprite = DogSprite;
        if(charactorNumber==1) MainSpriteRenderer.sprite = CatSprite;
    }

    // Update is called once per frame
    void Update()
    {

        //int key = 0;
        float workx = 0;
        float worky = 0;
        if (Input.GetKeyUp(KeyCode.D))
        {
            workx = 0;
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            workx = 0;
        }
        if (Input.GetKeyUp(KeyCode.W))
        {
            worky = 0;
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            worky = 0;
        }
        if (Input.GetKey(KeyCode.D))
        {
            //key = 1;
            workx = 0.03f;
            localScale = -0.19f;
        }
        if (Input.GetKey(KeyCode.A))
        {
           // key = -1;
            workx = -0.03f;
            localScale = 0.19f;
        }
        if (Input.GetKey(KeyCode.W))
        {
            if (roop == true) worky = 0.03f;
        }
        if (Input.GetKey(KeyCode.S))
        {
            if (roop == true) worky = -0.03f;
        }
        
        if (floorTaech == true)
        {
            if (Input.GetKeyDown(KeyCode.C))
            {
                floorTaech = false;
                this.rigid2D.AddForce(transform.up * jamp);
            }
        }

        if (transform.position.x > 11.0f || transform.position.x < -11.0f || transform.position.y > 5.0f || transform.position.y < -5.0f) this.DeadOrder();

        transform.localScale = new Vector3(localScale, 0.19f, 1);
        rigid2D = GetComponent<Rigidbody2D>();
        transform.Translate(workx, worky, 0);

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "BulletPrefab")
        {
            audio.PlayOneShot(hit);
            Destroy(collision.gameObject);
            this.DeadOrder();
        }
        if (collision.gameObject.tag == "Floor" || collision.gameObject.tag == "Frame")
        {
            floorTaech = true;
        }
    }
    //private void OnCollisionExit2D(Collision2D collision)
    //{
    //    if (collision.gameObject.tag == "Floor")
    //    {
    //        floorTaech = false;
    //    }
    //}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        roop = true;
        //rigid2D.bodyType = RigidbodyType2D.Kinematic;
        rigid2D.gravityScale = 0;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        roop = false;
        //rigid2D.bodyType = RigidbodyType2D.Dynamic;
        rigid2D.gravityScale = 1;
    }
    private void DeadOrder()
    {
        FindObjectOfType<GameDirector>().P1Point_();
        FindObjectOfType<P1Respown>().DRF();
        Destroy(gameObject);
    }
    private void Jamping()
    {
        floorTaech = false;
    }
}
