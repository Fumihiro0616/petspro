﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallteGen : MonoBehaviour
{
    public GameObject BulletPrefab;
    float posx = 0.5f;
    float posy = 0f;
 
    float key = 0.014652f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(KeyCode.RightArrow))
        {
            posx = 0.5f;
            posy = 0f;
            key = 0.014652f;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            posx = -0.5f;
            posy = 0f;
            key = -0.014652f;
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            posx = 0f;
            posy = 0.5f;

        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            posx = 0f;
            posy = -0.5f;
        }
        

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject Bullet = Instantiate(BulletPrefab) as GameObject;
            Bullet.transform.position = new Vector3(transform.position.x + posx, transform.position.y, transform.position.z);
            Bullet.transform.rotation = transform.rotation;

            Bullet.transform.localScale = new Vector3(key, 0.014652f, 0.014652f);
   
        }
    }

}
