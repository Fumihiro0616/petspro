﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P1Respown : MonoBehaviour
{
    public GameObject DogPrefab;
    float span = 3.0f;
    float delta = 0;
    bool dogResFlag = false;

    // Start is called before the first frame update
    void Start()
    {
        GameObject spown = Instantiate(DogPrefab) as GameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if(dogResFlag == true)
        {
            this.delta += Time.deltaTime;
            if (this.delta > this.span)
            {
                delta = 0;
                GameObject spown = Instantiate(DogPrefab) as GameObject;
                spown.transform.position = transform.position;
                dogResFlag = false;
            }
        }
    }

    public void DRF()
    {
        dogResFlag = true;
    }
}
