﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCon : MonoBehaviour
{
    bool OnLocationCommand = false;
    Rigidbody2D rigid2D;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        int key = 0;
        float workx = 0;
        float worky = 0;
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            OnLocationCommand = false;
        }

        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            OnLocationCommand = false;
        }

        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            OnLocationCommand = false;
        }

        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            OnLocationCommand = false;
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            key = 1;
            workx = 0.04f;
            OnLocationCommand = true;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            key = -1;
            workx = -0.04f;
            OnLocationCommand = true;
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            worky = 0.04f;
            OnLocationCommand = true;
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            worky = -0.04f;
            OnLocationCommand = true;
        }

        //歩く方向に応じて反転
        if (key != 0)
        {
            transform.localScale = new Vector3(0.405052f, 0.375057f, 1);
        }

        rigid2D = GetComponent<Rigidbody2D>();

        if (OnLocationCommand == false)
        {
            workx *= 0.98f;
            worky *= 0.98f;
        }

        transform.Translate(workx, worky, 0);

    }
}
