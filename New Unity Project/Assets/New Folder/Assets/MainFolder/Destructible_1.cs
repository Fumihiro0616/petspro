﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible_1 : MonoBehaviour
{
    int object_HitPoint = 1; //耐久値

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (object_HitPoint == 0)   //耐久値がなくなったら消去
        {
            Destroy(gameObject);
        }

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "BulletPrefab")    //当たった物が弾だったら耐久値が減る
        {
            object_HitPoint--;
        }
    }
}
