﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    public GameObject stage_1_Pre;
    public GameObject Stage;

    GameObject Dog_point;
    GameObject Cat_point;
    GameObject Txst;
    float delta = 0;
    int Minute = 1;
    bool timecount = true;
    public static int dogcount = 0;     //食らった回数
    public static int catcount = 0;
    public Text text;

    public static int player = 0;


    // Start is called before the first frame update
    void Start()
    {
        GameObject Result = GameObject.Find("ResultCanvas");
        
        TimeCountOn();  //カウント開始
        GameObject Bullet = Instantiate(Stage) as GameObject;
        Dog_point = GameObject.Find("Dog_Point");
        Cat_point = GameObject.Find("Cat_Point");
        Txst = GameObject.Find("Text");
    }

    // Update is called once per frame
    void Update()
    {
        //時間計測＆表示
        if (timecount == true)
        {
            this.delta -= Time.deltaTime;
            this.Txst.GetComponent<Text>().text = Minute + (".") + delta.ToString("F2");
            if (delta < 0 && Minute == 0)
            {
                this.TimeCountOff();
                SceneManager.LoadScene("endResult");
            }
            if (delta < 0)
            {
                Minute--;
                delta = 60;
            }

            //5回くらったらリザルトに遷移
            if (dogcount == 5)
            {
                this.TimeCountOff();
                SceneManager.LoadScene("endResult");
            }
            if (catcount == 5)
            {
                this.TimeCountOff();
                SceneManager.LoadScene("endResult");
            }
        }
    }

    //ダメージを食らったら呼ばれる
    public void P1Point_()
    {
        Dog_point.GetComponent<Image>().fillAmount -= 1.0f / 5;
        dogcount++;
    }
    public void P2Point_()
    {
        Cat_point.GetComponent<Image>().fillAmount -= 1.0f / 5;
        catcount++;
    }

    //勝ち負け判定
    public static int WinPlayer()
    {
        if (dogcount < catcount)
        {
            player = 0;
        }else if(dogcount > catcount){
            player = 1;
        }else{
            player = 2;
        }

        return player;

    }

    //多分ゲッター？
    public static int P1_Result()
    {
        return catcount;
    }
    public static int P2_Result()
    {
        return dogcount;
    }

    //終わったら時間を止めるかな？
    private void TimeCountOff()
    {
        timecount = false;
    }

    //カウント開始
    public void TimeCountOn()
    {
        timecount = true;
    }

    //初期化
    public static void CountReset()
    {
        dogcount = 0;
        catcount = 0;
    }

}
