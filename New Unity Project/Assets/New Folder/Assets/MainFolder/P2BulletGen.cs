﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P2BulletGen : MonoBehaviour
{
    public AudioClip shot;
    AudioSource audio;

    public GameObject BulletPrefab;
    float posx = -0.5f;
    float posy = 0f;
    float key = 0.014652f;
    float bulletSpeedx = -0.1f;
    float bulletSpeedy = 0.0f;
    float rateOfFire_Delay = 1.5f;
    private bool rateOfFire_Trigger = false;

    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
        //GameObject Bullet = Instantiate(BulletPrefab) as GameObject;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(KeyCode.RightArrow))
        {
            posx = 0.5f;
            posy = 0f;
            key = 0.014652f;
            bulletSpeedx = 0.1f;
            bulletSpeedy = 0.0f;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            posx = -0.5f;
            posy = 0f;
            key = -0.014652f;
            bulletSpeedx = -0.1f;
            bulletSpeedy = 0.0f;
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            posx = 0f;
            posy = 0.5f;
            bulletSpeedx = 0.0f;
            bulletSpeedy = 0.1f;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            posx = 0f;
            posy = -0.5f;
            bulletSpeedx = 0.0f;
            bulletSpeedy = -0.1f;
        }

        rateOfFire_Delay -= Time.deltaTime;
        if (rateOfFire_Delay > 0.0f) GoFire();


        if (Input.GetKeyDown(KeyCode.RightAlt))
        {
            if (rateOfFire_Trigger == true)
            {
                audio.PlayOneShot(shot);
                GameObject Bullet = Instantiate(BulletPrefab) as GameObject;
                Bullet.transform.position = new Vector3(transform.position.x + posx, transform.position.y + posy, transform.position.z);
                Bullet.transform.rotation = transform.rotation;
                Bullet.transform.localScale = new Vector3(key, 0.014652f, 0.014652f);
                FindObjectOfType<BulletCon>().BulletSpeedx(bulletSpeedx);
                FindObjectOfType<BulletCon>().BulletSpeedy(bulletSpeedy);
            }
        }
        if (Input.GetKeyUp(KeyCode.RightAlt))
        {
            StopFire();
        }
        


        void StopFire()
        {
            rateOfFire_Delay = 1.0f;
            rateOfFire_Trigger = false;
        }
        void GoFire()
        {
            rateOfFire_Trigger = true;
        }


    }
}