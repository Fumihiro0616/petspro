﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCon : MonoBehaviour
{
    float bulletSpeedx = 0.1f;
    float bulletSpeedy = 0.0f;
    Rigidbody2D rigid2D;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        rigid2D = GetComponent<Rigidbody2D>();
        transform.Translate(bulletSpeedx, bulletSpeedy, 0);
        if (transform.position.x > 11.0f|| transform.position.x < -11.0f || transform.position.y > 5.0f || transform.position.y < -5.0f)
            Destroy(gameObject);
    }

    public void BulletSpeedx(float v)
    {
        bulletSpeedx = v/2;
    }

    public void BulletSpeedy(float v)
    {
        bulletSpeedy = v/2;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Frame")
        {

        }
        else
        {
            Destroy(gameObject);
        }
    }
}
 
