﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface IPetsInterface
{
    //HP( キャラ差なしで30 )
    //int hitPoint = 30;

    //攻撃を行う関数
    void Shoot();

    //ダメージを食らった時の関数
    //第一引数 ... 与えた側の攻撃力
    //第二引数 ... 食らった側の防御力
    void AddDamege( int attack, int defense );

    //ペット固有のスキルを使ったときの処理
    void Skill();
}
