﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SelectCharactorScript : MonoBehaviour
{
    public static int p1Number = 1;
    public static int p2Number = 0;

    public Text text_1;
    GameObject Player1;

    public Text text_2;
    GameObject Player2;

    public AudioClip select;
    public AudioClip start;
    AudioSource audio;

    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
        Player1 = GameObject.Find("Text_1");
        Player2 = GameObject.Find("Text_2");
    }

    // Update is called once per frame
    void Update()
    {

    }

    //キャラの番号的なものを送っている？
    public static int Player_1SelectCharactor()
    {
        return p1Number;
    }
    public static int Player_2SelectCharactor()
    {
        return p2Number;
    }

    public void Push_Of_Selecting_1()
    {
        audio.PlayOneShot(select);
        p1Number = 1;
        p2Number = 0;
        this.Player1.GetComponent<Text>().text = "プレイヤー１";
        this.Player2.GetComponent<Text>().text = "プレイヤー２";
    }

    public void Push_Of_Selecting_2()
    {
        audio.PlayOneShot(select);
        p1Number = 0;
        p2Number = 1;
        this.Player1.GetComponent<Text>().text = "プレイヤー２";
        this.Player2.GetComponent<Text>().text = "プレイヤー１";
    }
    
    //プレイシーンに遷移
    public void Starting()
    {
        audio.PlayOneShot(start);
        SceneManager.LoadScene("PlayBattleSceen");
    }
}
