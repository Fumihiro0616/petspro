﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouse : MonoBehaviour
{
    private Vector3 position; // 位置座標
    private Vector3 screenToWorldPointPosition; // スクリーン座標をワールド座標に変換した位置座標
    void Start()
    {

    }
    void Update()
    {
        position = Input.mousePosition; // Vector3でマウス位置座標を取得する        
        position.z = 10f;// Z軸修正        
        screenToWorldPointPosition = Camera.main.ScreenToWorldPoint(position);// マウス位置座標をスクリーン座標からワールド座標に変換する
        gameObject.transform.position = screenToWorldPointPosition;// ワールド座標に変換されたマウス座標を代入
    }
}

