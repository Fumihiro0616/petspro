﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndResultText : MonoBehaviour
{
    int p1 = 0;
    int p2 = 0;
    int winPlayer = 0;

    public Sprite DowrSprite;
    public Sprite P1WinSprite;
    public Sprite P2WinSprite;

    GameObject ResultSprite;
    public Text player1;
    GameObject Player1;
    public Text player2;
    GameObject Player2;

    // Start is called before the first frame update
    void Start()
    {
        ResultSprite = GameObject.Find("Image");
        Player1 = GameObject.Find("Player1");
        Player2 = GameObject.Find("Player2");
        winPlayer = GameDirector.WinPlayer();
        p1 = GameDirector.P1_Result();
        p2 = GameDirector.P2_Result();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (p1 == p2) this.ResultSprite.GetComponent<Image>().sprite = DowrSprite;
        if (p1 > p2) this.ResultSprite.GetComponent<Image>().sprite = P1WinSprite;
        if (p1 < p2) this.ResultSprite.GetComponent<Image>().sprite = P2WinSprite;
        this.Player1.GetComponent<Text>().text = p1 + "";
        this.Player2.GetComponent<Text>().text = p2 + "";
    }

}
