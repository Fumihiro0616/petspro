﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ReSelectbutton : MonoBehaviour
{
    public void OnClick()
    {
        GameDirector.CountReset();
        SceneManager.LoadScene("SelectScene");
    }
}
