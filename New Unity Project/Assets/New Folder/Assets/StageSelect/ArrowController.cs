﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : MonoBehaviour
{
    private int selectStageNum = 0; 
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;
        
        if(Input.GetKeyDown(KeyCode.RightArrow) && pos.x <= 0)
        {
            transform.Translate(8, 0, 0);
            selectStageNum++;
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow) && pos.x >= 0)
        {
            transform.Translate(-8, 0, 0);
            selectStageNum--;
        }
        if (Input.GetKeyDown(KeyCode.UpArrow) && pos.y <= 0)
        {
            transform.Translate(0, 3, 0);
            selectStageNum -= 2;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow) && pos.y >= 0)
        {
            transform.Translate(0, -3, 0);
            selectStageNum += 2;
        }

    }

    public int GetSelectStageNum()
    {
        return selectStageNum;
    }
}
