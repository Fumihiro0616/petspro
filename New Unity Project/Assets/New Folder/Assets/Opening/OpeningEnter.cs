﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class OpeningEnter : MonoBehaviour
{
    private Image image;
    public  float speed = 1.0f;
    private float time;
    // Start is called before the first frame update
    void Start()
    {
        image = this.gameObject.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        image.color = GetAlphaColor(image.color);

        //左クリックでキャラ選択画面の遷移
        if(Input.GetMouseButtonUp(0))
        {
            SceneManager.LoadScene("SelectScene");
        }
    }

    //ClickStartの点滅表示
    Color GetAlphaColor(Color color)
    {
        time += Time.deltaTime * 5.0f * speed;
        color.a = Mathf.Sin(time);
        return color;
    }
}
